# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('articles', '0004_auto_20170602_0341'),
    ]

    operations = [
        migrations.AddField(
            model_name='article',
            name='version',
            field=models.CharField(max_length=40, default=1111),
            preserve_default=False,
        ),
    ]
