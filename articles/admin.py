from django.contrib import admin
from .models import Article, Node, Image

# Register your models here.


class ArticleAdmin(admin.ModelAdmin):

    list_display = ['title','user', 'slug', 'category','url', 'saved_on']
    list_filter = ['user', 'saved_on']
admin.site.register(Article, ArticleAdmin)
class NodeAdmin(admin.ModelAdmin):
	list_display=['article', 'node']
	list_filter = ['article', 'node']
admin.site.register(Node, NodeAdmin)
class ImageAdmin(admin.ModelAdmin):
	list_filter =['article']
admin.site.register(Image, ImageAdmin)

