from django.contrib import admin
from .models import Website

# Register your models here.


class WebsiteAdmin(admin.ModelAdmin):
    list_display = ['site_name', 'url', 'category', 'favicon_url']
    list_filter = ['created']


admin.site.register(Website, WebsiteAdmin)
