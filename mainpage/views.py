from django.shortcuts import render

# Create your views here.


def mainpage(request):

    return render(request, 'mainpage/main-page.html')


def howToSave(request):

    return render(request, 'mainpage/howToSave.html')
