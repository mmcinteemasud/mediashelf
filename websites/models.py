from django.db import models
from django.conf import settings
# Create your models here.

class Website(models.Model):
	user = models.ForeignKey(settings.AUTH_USER_MODEL,
                            related_name='website_saved')
	site_name = models.CharField(max_length=220)
	url = models.URLField(max_length=400)
	category = models.CharField(max_length=220)
	favicon_url = models.URLField(max_length=220)
	favicon = models.ImageField(upload_to='website_favicons/%Y/%m/%d', default='website_favicons/Internet-icon.png', blank=True)
	created = models.DateTimeField(auto_now_add=True,
                            db_index=True)
	version = models.CharField(max_length=40)
	class Meta:
    		ordering = ["-created"]
	def __str__(self):
		return self.site_name
