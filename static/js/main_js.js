
$(document).on("click", ".delete-item", function(e){
		e.preventDefault();
        var id = $(this).parent('div').parent().parent().find('.idHolder').attr('id');
        $.ajax({
            url: '/videos/video-delete/' + id + '/',
			type: 'POST',
			headers: {'X-CSRFToken': '{{ csrf_token }}'},
			dataType: 'json',
            success: function(response){
			    $(".table-responsive").load('/videos/');
				$(document).attr('location').href=document.URL;
				console.log(response["status"]);
            }
        });
    });

$(document).on("click", ".delete-image", function(e){
		e.preventDefault();
        var id = $(this).parent('div').parent().parent().find('.idHolder').attr('id');
        $.ajax({
            url: '/images/image-delete/' + id + '/',
			type: 'POST',
			headers: {'X-CSRFToken': '{{ csrf_token }}'},
			dataType: 'json',
            success: function(response){
			    $(".image-table-responsive").load('/images/');
				$(document).attr('location').href=document.URL;
				console.log(response["status"]);
            }
        });
    });

$(document).on("click", ".delete-article", function(e){
		e.preventDefault();
        var id = $(this).parent('div').parent().parent().find('.idHolder').attr('id');
        $.ajax({
            url: '/articles/article-delete/' + id + '/',
			type: 'POST',
			headers: {'X-CSRFToken': '{{ csrf_token }}'},
			data: '',
			dataType: 'json',
            success: function(response){
			    $(".article-table-responsive").load('/articles/');
				$(document).attr('location').href=document.URL;
				console.log(response["status"]);
            }
        });
    });

		$(document).on("click", ".delete-website", function(e){
				e.preventDefault();
		        var id = $(this).parent('div').parent().parent().find('.idHolder').attr('id');
		        $.ajax({
		            url: '/websites/website-delete/' + id + '/',
					type: 'POST',
					headers: {'X-CSRFToken': '{{ csrf_token }}'},
					data: '',
					dataType: 'json',
		            success: function(response){
					    $(".website-table-responsive").load('/websites/');
						$(document).attr('location').href=document.URL;
						console.log(response["status"]);
		            }
		        });
		    });

function deselect(e, form) {
    e.removeClass('selected');
	form.hide();
  $('.modal-backdrop').hide();
  };

$(function() {
  $(document).on('click', '.deleteButton',function() {
    if($(this).hasClass('selected')) {
      deselect($(this), $('.delete-form-mini-container'));
    } else {
      $(this).addClass('selected');

$('.delete-form-mini-container .idHolder').attr('id', $(this).attr('id'));
      $('.delete-form-mini-container').slideFadeToggle();
        $('.modal-backdrop').show();

    }
    return false;
  });
$(document).click(function(event) {
    if(!$(event.target).closest('.delete-form-mini-container').length) {
        if($('.delete-form-mini-container').is(":visible")) {
          deselect($('.deleteButton'), $('.delete-form-mini-container'));
          return false;
        }
    }
});

  $(document).on('click', '.modal_close', function() {
    deselect($('.deleteButton'), $('.delete-form-mini-container'));
    return false;
  });
  $(document).on('click', '.cancel', function() {
    deselect($('.deleteButton'), $('.delete-form-mini-container'));
    return false;
  });
});



$(function() {
  $(document).on('click', '.editButton',function() {
    if($(this).hasClass('selected')) {
      deselect($(this), $('.edit-form-mini-container'));
    } else {
      $(this).addClass('selected');
		console.log($(this).attr('id'));
	  $('.edit-form-mini-container .form-row input[name="title"]').val($.trim($(this).parent().parent('div').find('.info a.title').text()));
	  $('.edit-form-mini-container .form-row select[name="dropdown"]').val(($.trim($(this).parent().parent('div').find('.info a.category').text())));
$('.edit-form-mini-container img').attr('src', $(this).parent().parent('div').find('img').attr('src'));
$('.edit-form-mini-container .idHolder').attr('id', $(this).attr('id'));
      $('.edit-form-mini-container').slideFadeToggle();
      $('.modal-backdrop').show();

    }
    return false;
  });
$(document).click(function(event) {
    if(!$(event.target).closest('.edit-form-mini-container').length) {
        if($('.edit-form-mini-container').is(":visible")) {
					deselect($('.editButton'), $('.edit-form-mini-container'));
					return false;
        }
    }
});
  $(document).on('click', '.edit-modal-close', function() {
    deselect($('.editButton'), $('.edit-form-mini-container'));
    return false;
  });
});


$(function() {

  $(document).on('click', '.friendButton',function() {
    if($(this).hasClass('selected')) {
      deselect($(this), $('.friend-form-mini-container'));
    } else {
      $(this).addClass('selected');
		console.log($(this).attr('id'));
$('.friend-form-mini-container img').attr('src', $(this).parent().parent('div').find('img').attr('src'));
$('.friend-form-mini-container .idHolder').attr('id', $(this).attr('id'));
$('.friend-form-mini-container .friend-form-mini .form-row #fb').attr('href', $(this).parent().parent('div').find('.friend-div .fb-post .facebook-this a').attr('href'));
$('.friend-form-mini-container .friend-form-mini .form-row #twitter').attr('href', $(this).parent().parent('div').find('.friend-div .twitter-post .tweet-this a').attr('href'));
$('.friend-form-mini-container .friend-form-mini .form-row #email').attr('href', $(this).parent().parent('div').find('.friend-div .email-post .mail-this a').attr('href'));
      $('.friend-form-mini-container').slideFadeToggle();
			$('.modal-backdrop').show();
    }
    return false;
  });
$(document).click(function(event) {

    if(!$(event.target).closest('.friend-form-mini-container').length) {
        if($('.friend-form-mini-container').is(":visible")) {
					deselect($('.friendButton'), $('.friend-form-mini-container'));
			    return false;
        }
    }
});

  $(document).on('click', '.friend-modal-close', function() {
    deselect($('.friendButton'), $('.friend-form-mini-container'));
    return false;
  });
});


$.fn.slideFadeToggle = function(easing, callback) {
  return this.animate({ opacity: 'toggle'}, 'fast', easing, callback);

};

$.fn.positionPopup = function(e){
	this.css({
        'position': 'absolute',
        'left': $(e).parent().offset().left - .5*($(this).width()) + .5*($(e).parent().width()),
        'top': $(e).offset().top + $(e).height() + 5
   });
	var pos = this.offset();
	if ((pos.top + this.height()) > $(window).height()){
			this.css({
        'position': 'absolute',
        'left': $(e).parent().offset().left - .5*($(this).width()) + .5*($(e).parent().width()),
        'top': pos.top - (pos.top + this.height() - $(window).height())
   });

}
};
$(document).on("click", "#editVideoSubmit", function(e){
		e.preventDefault();

        var id = $(this).parent('div').parent().parent().find('.idHolder').attr('id');
		var items = [];
		var vidItems = {
			title: $(this).parent('div').parent().find('.form-row input[name="title"]').val(),
			category: $(this).parent('div').parent().find('.form-row select[name="dropdown"]').val()
};

		items.push(vidItems);
		items = JSON.stringify(items);
        $.ajax({
            url: '/videos/video-edit/' + id + '/',
			type: 'POST',
			headers: {'X-CSRFToken': '{{ csrf_token }}'},
			data: items,
			dataType: 'json',
            success: function(response){
			    $(".table-responsive").load('/videos/');
				console.log(response["status"]);
				$(document).attr('location').href=document.URL;
            }
        });

    });


    $(document).on("click", "#editWebsiteSubmit", function(e){
    		e.preventDefault();

            var id = $(this).parent('div').parent().parent().find('.idHolder').attr('id');
    		var items = [];
    		var websiteItems = {
    			site_name: $(this).parent('div').parent().find('.form-row input[name="title"]').val(),
    			category: $(this).parent('div').parent().find('.form-row select[name="dropdown"]').val()
    };

    		items.push(websiteItems);
    		items = JSON.stringify(items);
            $.ajax({
                url: '/websites/website-edit/' + id + '/',
    			type: 'POST',
    			headers: {'X-CSRFToken': '{{ csrf_token }}'},
    			data: items,
    			dataType: 'json',
                success: function(response){
    			    $(".website-table-responsive").load('/websites/');
    				console.log(response["status"]);
    				$(document).attr('location').href=document.URL;
                }
            });

        });


$(function() {
  $(document).on('click', '.articleEditButton',function() {
    if($(this).hasClass('selected')) {
      deselect($(this), $('.edit-form-mini-container'));
    } else {
      $(this).addClass('selected');
	  $('.edit-form-mini-container .form-row input[name="title"]').val($.trim($(this).parent().parent('div').find('.info a.title').text()));
	  $('.edit-form-mini-container .form-row select[name="dropdown"]').val(($.trim($(this).parent().parent('div').find('.info a.category').text())));
$('.edit-form-mini-container img').attr('src', $(this).parent().parent('div').find('img').attr('src'));
$('.edit-form-mini-container .idHolder').attr('id', $(this).attr('id'));
      $('.edit-form-mini-container').slideFadeToggle();
      $('.modal-backdrop').show();
    }
    return false;
  });
$(document).click(function(event) {
    if(!$(event.target).closest('.edit-form-mini-container').length) {
        if($('.edit-form-mini-container').is(":visible")) {
          deselect($('.articleEditButton'), $('.edit-form-mini-container'));
          return false;
        }
    }
});

  $(document).on('click', '.edit-modal-close', function() {
    deselect($('.articleEditButton'), $('.edit-form-mini-container'));
    return false;
  });
});

$(document).on("click", "#editArticleSubmit", function(e){
		e.preventDefault();

        var id = $(this).parent('div').parent().parent().find('.idHolder').attr('id');
		var items = [];
		var vidItems = {
			title: $(this).parent('div').parent().find('.form-row input[name="title"]').val(),
			category: $(this).parent('div').parent().find('.form-row select[name="dropdown"]').val()
};
		items.push(vidItems);
		items = JSON.stringify(items);
        $.ajax({
            url: '/articles/article-edit/' + id + '/',
			type: 'POST',
			headers: {'X-CSRFToken': '{{ csrf_token }}'},
			data: items,
			dataType: 'json',
            success: function(response){
			    $(".article-table-responsive").load('/articles/');
				console.log(response["status"]);
				$(document).attr('location').href=document.URL;
            }
        });

    });

$(function() {
  $(document).on('click', '.imageEditButton',function() {
    if($(this).hasClass('selected')) {
      deselect($(this), $('.image-edit-form-mini-container'));
    } else {
      $(this).addClass('selected');
	  $('.image-edit-form-mini-container .form-row input[name="title"]').val($.trim($(this).parent().parent('div').find('.info a.title').text()));
	  $('.image-edit-form-mini-container .form-row textarea').val($.trim($(this).parent().parent('div').find('.info .description').text()));
$('.image-edit-form-mini-container img').attr('src', $(this).parent().parent('div').find('img').attr('src'));
$('.image-edit-form-mini-container .idHolder').attr('id', $(this).attr('id'));
      $('.image-edit-form-mini-container').slideFadeToggle();
      $('.modal-backdrop').show();
    }
    return false;
  });
$(document).click(function(event) {
    if(!$(event.target).closest('.image-edit-form-mini-container').length) {
        if($('.image-edit-form-mini-container').is(":visible")) {
          deselect($('.imageEditButton'), $('.image-edit-form-mini-container'));
          return false;
        }
    }
});

  $(document).on('click', '.edit-modal-close', function() {
    deselect($('.imageEditButton'), $('.image-edit-form-mini-container'));
    return false;
  });
});

$(document).on("click", "#editImageSubmit", function(e){
		e.preventDefault();

        var id = $(this).parent('div').parent().parent().find('.idHolder').attr('id');
		var items = [];
		var imgItems = {
			title: $(this).parent('div').parent().find('.form-row input[name="title"]').val(),
			description: $(this).parent('div').parent().find('.form-row textarea').val()
};
		items.push(imgItems);
		items = JSON.stringify(items);
        $.ajax({
            url: '/images/image-edit/' + id + '/',
			type: 'POST',
			headers: {'X-CSRFToken': '{{ csrf_token }}'},
			data: items,
			dataType: 'json',
            success: function(response){
			    $(".image-table-responsive").load('/images/');
				$(document).attr('location').href=document.URL;
				console.log(response["status"]);
            }
        });

    });
		//Filter items with radio button
		$(document).on("click", ".item input[name='action']", function() {
      filter_item= $('input[name=action]:checked').val();
        $(document).on('click', '#filter_button', function() {
          $('.image').each(function(){
            $(this).show();
            if (filter_item == 'All'){
              $(this).show();
            }
            else if ($.trim($(this).children().find('.category').text()) != filter_item){
              $(this).hide();
            }
          });
        });
		});

    function isEmpty( el ){
        return !$.trim(el.html())
    }
$(document).ready(function(){
  if (isEmpty($('#slider-list'))) {
    $('#slider-list').hide();
    $('#slider').append('<li><img id="banner_default" src="" width="750" height="325"></li>').attr('style', 'list-style: none');
    $('#banner_default').attr('src', banner_default);


}


});



var BrowserDetect = {
        init: function () {
            this.browser = this.searchString(this.dataBrowser) || "Other";
            this.version = this.searchVersion(navigator.userAgent) || this.searchVersion(navigator.appVersion) || "Unknown";
        },
        searchString: function (data) {
            for (var i = 0; i < data.length; i++) {
                var dataString = data[i].string;
                this.versionSearchString = data[i].subString;

                if (dataString.indexOf(data[i].subString) !== -1) {
                    return data[i].identity;
                }
            }
        },
        searchVersion: function (dataString) {
            var index = dataString.indexOf(this.versionSearchString);
            if (index === -1) {
                return;
            }

            var rv = dataString.indexOf("rv:");
            if (this.versionSearchString === "Trident" && rv !== -1) {
                return parseFloat(dataString.substring(rv + 3));
            } else {
                return parseFloat(dataString.substring(index + this.versionSearchString.length + 1));
            }
        },

        dataBrowser: [
            {string: navigator.userAgent, subString: "Edge", identity: "MS Edge"},
            {string: navigator.userAgent, subString: "MSIE", identity: "Explorer"},
            {string: navigator.userAgent, subString: "Trident", identity: "Explorer"},
            {string: navigator.userAgent, subString: "Firefox", identity: "Firefox"},
            {string: navigator.userAgent, subString: "Opera", identity: "Opera"},  
            {string: navigator.userAgent, subString: "OPR", identity: "Opera"},  

            {string: navigator.userAgent, subString: "Chrome", identity: "Chrome"}, 
            {string: navigator.userAgent, subString: "Safari", identity: "Safari"}       
        ]
    };
    
    BrowserDetect.init();



if (BrowserDetect.browser == 'Firefox'){
	$('#chrome_extension').hide();
	$('#safari_extension').hide();
};
if (BrowserDetect.browser == 'Chrome'){
	$('#firefox_extension').hide();
	$('#safari_extension').hide();
	var chrome_extension = document.getElementById('chrome_extension');
	$(chrome_extension).on('click', function(){
		chrome.webstore.install();
	});
	if (chrome.app.isInstalled) {
  		alert("MediaShelf extension is already installed!");
}
};
if (BrowserDetect.browser == 'Safari'){
	$('#chrome_extension').hide();
	$('#firefox_extension').hide();
}

// Toggle .dropdown-active on click
$('#drop-down-menu').on('click', function (event) {
		event.stopPropagation();
		$('.dropdown-menu').toggleClass('dropdown-active');
    $('.filter-dropdown').removeClass('dropdown-active');
    $('.search-dropdown').removeClass('dropdown-active');
});

$('#filter').on('click', function (event) {
		event.stopPropagation();
		$('.filter-dropdown').toggleClass('dropdown-active');
    $('.dropdown-menu').removeClass('dropdown-active');
    $('.search-dropdown').removeClass('dropdown-active');
});

$('#mobile-search-icon').on('click', function (event) {
		event.stopPropagation();
    $('.search-dropdown').toggleClass('dropdown-active');
		$('.filter-dropdown').removeClass('dropdown-active');
    $('.dropdown-menu').removeClass('dropdown-active');
});
