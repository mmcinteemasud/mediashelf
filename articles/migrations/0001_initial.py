# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
from django.conf import settings
import imagekit.models.fields


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Article',
            fields=[
                ('id', models.AutoField(serialize=False, verbose_name='ID', primary_key=True, auto_created=True)),
                ('title', models.CharField(max_length=200)),
                ('slug', models.SlugField(max_length=200, blank=True)),
                ('category', models.CharField(max_length=200)),
                ('url', models.URLField(max_length=400)),
                ('saved_on', models.DateTimeField(auto_now_add=True)),
                ('user', models.ForeignKey(related_name='articles_saved', to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'ordering': ['-saved_on'],
            },
        ),
        migrations.CreateModel(
            name='Image',
            fields=[
                ('id', models.AutoField(serialize=False, verbose_name='ID', primary_key=True, auto_created=True)),
                ('image_url', models.TextField(max_length=400)),
                ('image', imagekit.models.fields.ProcessedImageField(upload_to='article_images/%Y/%m/%d', blank=True)),
                ('article', models.ForeignKey(to='articles.Article', related_name='image', null=True)),
            ],
        ),
        migrations.CreateModel(
            name='Node',
            fields=[
                ('id', models.AutoField(serialize=False, verbose_name='ID', primary_key=True, auto_created=True)),
                ('node', models.IntegerField(default=0)),
                ('html_element', models.CharField(max_length=200)),
                ('content', models.TextField(max_length=800, default='blank')),
                ('article', models.ForeignKey(to='articles.Article', related_name='article', null=True)),
                ('image', models.ForeignKey(to='articles.Image', blank=True, related_name='node', null=True)),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL, related_name='user', default=1)),
            ],
            options={
                'ordering': ('node',),
            },
        ),
    ]
