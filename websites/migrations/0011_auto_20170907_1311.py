# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('websites', '0010_auto_20170907_1308'),
    ]

    operations = [
        migrations.AlterField(
            model_name='website',
            name='favicon',
            field=models.ImageField(blank=True, default='website_favicons/Internet-icon.png', upload_to='website_favicons/%Y/%m/%d'),
        ),
    ]
