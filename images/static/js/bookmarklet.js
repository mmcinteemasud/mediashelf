(function(){
  var jquery_version = '2.1.4';
  var site_url = 'http://mysite.com:8000/';
  var static_url = site_url + 'static/';
  var min_width = 100;
  var min_height = 100;

  function bookmarklet(msg) {
    // load CSS
    var css = jQuery('<link>');
	var css2 = jQuery('<link>');
    css.attr({
      rel: 'stylesheet',
      type: 'text/css',
      href: static_url + 'css/bookmarklet.css?r=' + Math.floor(Math.random()*99999999999999999999)
    },
{
      rel: 'stylesheet',
      type: 'text/css',
      href: static_url + 'css/font-awesome-4.7.0/css/font-awesome.min.css'
    },
{
rel:'icon',
type: 'image/png',
href: static_url + 'css/icons/media_shelf1.png'
});
    jQuery('head').append(css);
	

    // load HTML
    box_html = '<div id="bookmarklet"><div id="header"><div class="logo"></div></div><div id="container"><div id="images"></div><div id="article_categories"></div><div id="video_categories"></div><div id="website_categories"></div><div id="status_display"></div><div id="get_buttons"><p>What would you like to save?</p><span class="button"><button id="image_button">Images</button></span><span class="button"><button id="article_button">Article</button></span><span class="button"><button id="video_button">Video</button></span><span class="button"><button id="website_button">WebSite</button></span></div><br clear="all" /></div><i id="close" class="fa fa-window-close"></i></div>';
    jQuery('body').append(box_html);

	  // close event
	  jQuery('#bookmarklet #close').click(function(){
      jQuery('#bookmarklet').remove();
	  });

// Send a message containing the page details back to the event page


article = [];
video = [];
website = [];
var get_video;
var img_url;
var favicon;

//validate and clean url
function validUrl(test_url) {
    if (test_url) {
        if (/^(http|https|ftp):\/\/[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$/i.test(test_url)) {
            return true;
            //alert("valid URL");
            //console.log(test_url);

        } 

		if (test_url.substring(0,2)=='//'){
			img_url = location.protocol + test_url;
		
		}
		else 
if (test_url.substring(0,1)=='/'){
			img_url = location.protocol + '//' + location.host + test_url;
		
		}
else {
            img_url = location.protocol + '//' + location.host + '/' + test_url;
            //alert("invalid URL");
            //console.log(img_url);
        }
    } else {
        img_url = "";
    }
}

//validate and clean favicon url
function validFaviconUrl(test_url) {
    if (test_url) {
        if (/^(http|https|ftp):\/\/[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$/i.test(test_url)) {
            return true;
            //alert("valid URL");
            //console.log(test_url);

        } 

		if (test_url.substring(0,2)=='//'){
			favicon = location.protocol + test_url;
		
		}
		else 
if (test_url.substring(0,1)=='/'){
			favicon = location.protocol + '//' + location.host + test_url;
		
		}
else {
            favicon = location.protocol + '//' + location.host + '/' + test_url;
            //alert("invalid URL");
            //console.log(img_url);
        }
    } else {
        favicon = "";
    }
}
//get favicon 

var getFavicon = function(){
    var nodeList = document.getElementsByTagName("link");
    for (var i = 0; i < nodeList.length; i++)
    {
        if((nodeList[i].getAttribute("rel") == "icon")||(nodeList[i].getAttribute("rel") == "shortcut icon"))
        {
            favicon = nodeList[i].getAttribute("href");
        }
    }
    return favicon;        
}
getFavicon();
validFaviconUrl(favicon);


//get article, image, site info
var title = $(document).attr('title');
$('body').find("img:hidden").show();
var get_article = $('body').find(":hidden").remove();


var get_article = $(get_article).ready(function() {

    $('img').each(function() {
        if ($(this).width() < 95 || $(this).height() < 121) {
            $(this).detach();
        }
    });

    $('p').each(function() {
        if ($(this).is(':contains("Advertisement")')) {
            $(this).detach();
        }

        if ($(this).is(':contains("ADVERTISEMENT")')) {
            $(this).detach();
        }

    });
    return get_article;
});
vid_img_url = $('body').find('img[src$=".jpg"]:first').attr("src");
var get_article = $('p, img');




var header_items = {
    title: title,
    category: " ",
    url: $(location).attr('href'),
	favicon_url: favicon
};
article.push(header_items);


for (i = 0; i < get_article.length; i++) {
    if ($(get_article[i]).is('p')) {
        var p_items = {
            node: i,
            html_element: "p",
            content: $(get_article[i]).text()
        };
        article.push(p_items);
    }
    if ($(get_article[i]).is('img')) {
        if ($(get_article[i]).attr("src")) {
          if (/^(http|https|ftp):\/\/[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$/i.test($(get_article[i]).attr("src"))) {
            var img_items = {
                node: i,
                html_element: "img",
                content: $(get_article[i]).attr("src")
            };
            article.push(img_items);

          }
}
         else if ($(get_article[i]).attr("srcset")) {
            if (/^(http|https|ftp):\/\/[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$/i.test($(get_article[i]).attr("srcset"))) {
              var img_items = {
                  node: i,
                  html_element: "img",
                  content: $(get_article[i]).attr("srcset").split(' ')[0]
              };
              article.push(img_items);
        }
    }
}

if (window.location.href.indexOf("youtube") != -1) {
    console.log("this is youtube");
    var regExp = /.*(?:youtu.be\/|v\/|u\/\w\/|embed\/|watch\?v=)([^#\&\?]*).*/;
    var match = window.location.href.match(regExp);
    if (match && match[1].length == 11) {
        urllink = match[1];
        vid_img_url = "http:\/\/img.youtube.com\/vi\/" + urllink + "\/hqdefault.jpg";
    }
} else if (window.location.href.indexOf("vimeo.com") != -1) {
    console.log("this is vimeo");
    var regExp = /.*(www\.)?vimeo.com\/(\d+)($|\/)/;
    var match = window.location.href.match(regExp);
    if (match) {
        video_id = match[2];
        var getVimeoThumbnail = function(id) {
            $.ajax({
                type: 'GET',
                url: 'https://vimeo.com/api/v2/video/' + id + '.json',
                jsonp: 'callback',
                dataType: 'jsonp',
                async: false,
                success: function(data) {
                    vid_img_url = (data[0].thumbnail_large);
                }
            });
        };
        getVimeoThumbnail(video_id);
    }
} 

else {
    vid_img_url = img_url;
}
}

//validate and clean vid_img_url
validUrl(img_url);

var get_video = {
    title: $('title').text(),
    url: $(location).attr('href'),
    category: " ",
    image_url: img_url

};
video.push(get_video);
var images = $(get_article).filter('img').map(function() {
    return this.src;
}).toArray();

var get_website = {
	site_name: location.host,
	url: $(location).attr('href'),
	category: " ",
	favicon_url: favicon
};
website.push(get_website);
console.log(website[0]);


window.location.reload();

  };
  // Check if jQuery is loaded
  if(typeof window.jQuery != 'undefined') {
    bookmarklet();
  } else {
    // Check for conflicts
    var conflict = typeof window.$ != 'undefined';
    // Create the script and point to Google API
    var script = document.createElement('script');
    script.setAttribute('src','http://ajax.googleapis.com/ajax/libs/jquery/' + jquery_version + '/jquery.min.js');
    // Add the script to the 'head' for processing
    document.getElementsByTagName('head')[0].appendChild(script);
    // Create a way to wait until script loading
    var attempts = 15;
    (function(){
      // Check again if jQuery is undefined
      if(typeof window.jQuery == 'undefined') {
        if(--attempts > 0) {
          // Calls himself in a few milliseconds
          window.setTimeout(arguments.callee, 250)
        } else {
          // Too much attempts to load, send error
          alert('An error ocurred while loading jQuery')
        }
      } else {
          bookmarklet();
      }
    })();
  }

})()
