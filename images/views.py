from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from django.contrib import messages
from common.search import *
from .forms import ImageCreateForm
from django.shortcuts import get_object_or_404
from .models import Image
import json
from django.http import JsonResponse
from django.views.decorators.http import require_POST
from common.decorators import ajax_required
from django.utils.text import slugify
from actions.utils import create_action
from images.serializers import ImageSerializer
from rest_framework import generics
from rest_framework.response import Response
from rest_framework import status
from rest_framework.authentication import TokenAuthentication
from rest_framework.permissions import IsAuthenticated
import random
import hashlib
# Create your views here.
@login_required
def image_create(request):
    version = hashlib.sha1((str(random.random())).encode('utf8')).hexdigest()[:5]
    if request.method == 'POST':
        # form is sent
        form = ImageCreateForm(data=request.POST)
        if form.is_valid():
            # form data is valid
            cd = form.cleaned_data
            new_item = form.save(commit=False)

            # assign current user to the item
            new_item.user = request.user
            new_item.version = version
            new_item.save()
            create_action(request.user, 'Saved: ' + new_item.title, 'Image', new_item)
            messages.success(request, 'Image added successfully')

            # redirect to new created item detail view
            return redirect(new_item.get_absolute_url())
    else:
        # build form with data provided by the bookmarklet via GET
        form = ImageCreateForm(data=request.GET)

    return render(request,
                'images/image/create.html',
                {'section': 'images',
                'form': form})

def image_detail(request, id, slug):
    image = get_object_or_404(Image, id=id, slug=slug)
    return render(request,
                'images/image/detail.html',
                {'section': 'images',
                'image': image})

@login_required
def image_list(request):
    user = request.user
    images = Image.objects.filter(user= user)
    if request.is_ajax():
        return render(request,
                        'images/image/responsive-image-list.html',
                        {'section': 'images', 'images': images})
    return render(request,
                    'images/image/list.html',
                    {'section': 'images', 'images': images})

@ajax_required
@login_required
def image_edit(request, image_id):
	data = json.loads(request.body.decode("utf-8"))
	user = request.user
	title = data[0]['title']
	description = data[0]['description']
	version = hashlib.sha1((str(random.random())).encode('utf8')).hexdigest()[:5]
	image = Image.objects.get(pk=image_id)
	image.title=title
	image.slug=slugify(title)
	image.description=description
	image.version=version
	image.save()
	return JsonResponse({'status':'success'})


@ajax_required
@login_required
def image_delete(request, image_id):
    image = Image.objects.get(pk=image_id)
    create_action(request.user, 'Deleted: ' + image.title, 'Image',  image)
    image.delete()
    return JsonResponse({'status': 'Deleted successfuly!'})


@ajax_required
@login_required
@require_POST
def image_like(request):
    image_id = request.POST.get('id')
    action = request.POST.get('action')
    if image_id and action:
        try:
            image = Image.objects.get(id=image_id)
            if action == 'like':
                image.users_like.add(request.user)
            else:
                image.users_like.remove(request.user)
            return JsonResponse({'status':'ok'})
        except:
            pass
    return JsonResponse({'status':'ko'})


@login_required
def search(request):
	query_string = ''
	images = None
	if ('q' in request.GET) and request.GET['q'].strip():
		query_string = request.GET['q']
		entry_query = get_query(query_string, ['title', 'description' ])
		images = Image.objects.filter(entry_query)
	return render(request,'images/image/list.html',{'section': 'images', 'images': images})

class ImageList(generics.ListCreateAPIView):
    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)
    serializer_class = ImageSerializer
    def get_queryset(self):
        user = self.request.user
        return Image.objects.filter(user=user)

class ImageDetail(generics.RetrieveUpdateDestroyAPIView):
    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)
    serializer_class = ImageSerializer
    def get_queryset(self):
        user = self.request.user
        return Image.objects.filter(user=user)
