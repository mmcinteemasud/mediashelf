# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('videos', '0005_auto_20170429_2147'),
    ]

    operations = [
        migrations.AlterField(
            model_name='video',
            name='image_url',
            field=models.URLField(max_length=400),
        ),
        migrations.AlterField(
            model_name='video',
            name='url',
            field=models.URLField(max_length=400),
        ),
    ]
