# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('videos', '0004_auto_20170429_2145'),
    ]

    operations = [
        migrations.AlterField(
            model_name='video',
            name='image',
            field=models.ImageField(blank=True, upload_to='video_images/%Y/%m/%d', default='video_images/default_video_img.jpg'),
        ),
    ]
