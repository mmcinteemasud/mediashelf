# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('images', '0003_auto_20170214_1941'),
    ]

    operations = [
        migrations.AlterField(
            model_name='image',
            name='url',
            field=models.TextField(max_length=400),
        ),
    ]
