# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('websites', '0007_auto_20170509_2202'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='website',
            options={'ordering': ['-created']},
        ),
    ]
