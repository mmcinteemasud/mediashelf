from django.conf.urls import url
from . import views


urlpatterns = [
    url(r'^create/$', views.image_create, name='create'),
    url(r'^detail/(?P<id>\d+)/(?P<slug>[-\w]+)/$',
        views.image_detail, name='detail'),
    url(r'^like/$', views.image_like, name='like'),
    url(r'^$', views.image_list, name='list'),
    url(r'^image-edit/(?P<image_id>\d+)/$', views.image_edit, name='image_edit'),
    url(r'^image-delete/(?P<image_id>\d+)/$', views.image_delete, name='image_delete'),
    url(r'^search/$', views.search, name='image-search'),
]
