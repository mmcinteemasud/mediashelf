# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Website',
            fields=[
                ('id', models.AutoField(serialize=False, primary_key=True, auto_created=True, verbose_name='ID')),
                ('site_name', models.CharField(max_length=220)),
                ('url', models.URLField(max_length=220)),
                ('category', models.CharField(max_length=220)),
                ('favicon_url', models.URLField(max_length=220)),
                ('favicon', models.ImageField(upload_to='website_favicons/%Y/%m/%d')),
                ('created', models.DateField(auto_now_add=True, db_index=True)),
                ('user', models.ForeignKey(related_name='website_saved', to=settings.AUTH_USER_MODEL)),
            ],
        ),
    ]
