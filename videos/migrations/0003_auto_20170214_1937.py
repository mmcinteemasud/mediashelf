# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('videos', '0002_auto_20170106_1500'),
    ]

    operations = [
        migrations.AlterField(
            model_name='video',
            name='image',
            field=models.ImageField(upload_to='video_images/%Y/%m/%d', default='video_images/default_video_img.jpg', blank=True),
        ),
    ]
