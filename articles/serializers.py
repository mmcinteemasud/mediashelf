from rest_framework import serializers
from articles.models import Article, Node, Image



class ImageSerializer(serializers.ModelSerializer):
    class Meta:
        model = Image
        fields = ('image',)



class NodeSerializer(serializers.ModelSerializer):
    image = ImageSerializer()

    class Meta:
        model = Node
        fields = ('id', 'node', 'html_element', 'content', 'image')


class ArticleSerializer(serializers.ModelSerializer):
    node = NodeSerializer(many=True)
    class Meta:
        model = Article
        fields = ('id', 'title', 'slug', 'category', 'url', 'saved_on', 'version', 'node')
