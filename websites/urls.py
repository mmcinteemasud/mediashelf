from django.conf.urls import url,patterns
from . import views



urlpatterns = [
	url(r'^website/$', views.websiteSave, name='websiteSave'),
	url(r'^mobile-save/$', views.mobile_save, name='mobile-save'),
	url(r'^$', views.websiteList, name='list'),
    url(r'^website-delete/(?P<website_id>\d+)/$', views.website_delete, name='website_delete'),
    url(r'^website-edit/(?P<website_id>\d+)/$', views.website_edit, name='website_edit'),
    url(r'^search/$', views.search, name='website-search'),
]
