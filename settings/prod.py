from settings.common import *



DEBUG = True
THUMBNAIL_DEBUG = False
SOCIAL_AUTH_REDIRECT_IS_HTTPS= True
AWS_HEADERS = {  # see http://developer.yahoo.com/performance/rules.html#expires
        'Expires': 'Thu, 31 Dec 2099 20:00:00 GMT',
        'Cache-Control': 'max-age=94608000',
    }
ALLOWED_HOSTS=['*']

BASE_URL = 'https://getmediashelf.com'
INSTALLED_APPS += ('storages',)
AWS_STORAGE_BUCKET_NAME = 'staticmediashelf'
AWS_MEDIA_BUCKET_NAME = 'mediashelfbucket'
STATICFILES_STORAGE = 'storages.backends.s3boto.S3BotoStorage'
DEFAULT_FILE_STORAGE = 'custom_storages.MediaStorage'
S3_URL = 'https://%s.s3.amazonaws.com/' % AWS_STORAGE_BUCKET_NAME
S3_MEDIA_URL = 'https://%s.s3.amazonaws.com/' % AWS_MEDIA_BUCKET_NAME
STATIC_URL = S3_URL
MEDIA_URL = S3_MEDIA_URL 
with open('/etc/db_pwd.txt') as p:
    db_pwd = p.read().strip()

EMAIL_USE_TSL = False
EMAIL_USE_SSL = True
EMAIL_HOST = 'smtp.zoho.com'
EMAIL_PORT = 465
EMAIL_HOST_USER = 'admin@getmediashelf.com'
EMAIL_HOST_PASSWORD = db_pwd
DEFAULT_FROM_EMAIL = 'MediaShelf <admin@getmediashelf.com>'

DATABASES = {    
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'mediashelf',
        'USER' : 'mcinteemasud',
        'PASSWORD' : db_pwd,
        'HOST' : 'mediashelf0618.c91qocepefcf.us-west-2.rds.amazonaws.com',
        'PORT' : '5432',
    }
}
