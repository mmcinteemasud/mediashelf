# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('articles', '0002_remove_node_user'),
    ]

    operations = [
        migrations.AlterField(
            model_name='node',
            name='article',
            field=models.ForeignKey(null=True, to='articles.Article', related_name='node'),
        ),
    ]
