# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('websites', '0005_auto_20170429_2210'),
    ]

    operations = [
        migrations.AlterField(
            model_name='website',
            name='favicon',
            field=models.ImageField(blank=True, upload_to='website_favicons/%Y/%m/%d', default='website_favicons/internet.ico'),
        ),
    ]
