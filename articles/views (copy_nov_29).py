from django.contrib.auth.decorators import login_required
from django.shortcuts import render, redirect
from django.utils import timezone
import json
from django.shortcuts import get_object_or_404
from django.http import JsonResponse
from django.http import HttpResponse
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from .models import Article, Node, Image
from django.core.files import File
from urllib.request import urlopen
from urllib.parse import urlparse
from urllib import request
from django.core.files.base import ContentFile
import urllib.request
# Create your views here.

@login_required
def article_save(request):
	data = (json.loads(request.read().decode('utf-8')))
	user = request.user
	title = data[0]['title']
	category = data[0]['category']
	url = data[0]['url']
	print('ok')

	print('ok2')
	article_instance= Article.objects.create(user=user, title=title, category=category, url=url)
	print('ok3')
	for i in range (1, len(data)):
		if data[i]['html_element']=='img':
			print(data[i])
			node=data[i]['node']
			image_url = data[i]['content']
			html_element=data[i]['html_element']
			Node.objects.create(article=article_instance, node=node, content=image_url, html_element=html_element)
			class AppURLopener(urllib.request.FancyURLopener):
				version = "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/35.0.1916.47 Safari/537.36"
			print('ok4')
			opener = AppURLopener()
			name = urlparse(image_url).path.split('/')[-1]
			response = opener.open(image_url)
			image_content = ContentFile(response.read())
			image_instance = Image.objects.create(article=article_instance, node=node, image_url=image_url)
			image_instance.image.save(name, image_content)
		else:
			Node.objects.create(article=article_instance, node=data[i]['node'], content=data[i]['content'], html_element=data[i]['html_element'])
			print("element", i)
	print(article_instance.slug)
	return HttpResponse(json.dumps({}),content_type="application/json")

@login_required
def article_detail(request, id, slug):
	article = get_object_or_404(Article, id=id, slug=slug)
	print('Hit')
	return render(request, 'articles/article_view.html',
{'section': 'articles','article': article})

@login_required
def article_list(request): 
    user = request.user
    articles = Article.objects.filter(user= user)
    paginator = Paginator(articles, 8)
    page = request.GET.get('page')
    try:
        articles = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer deliver the first page
        articles = paginator.page(1)
    except EmptyPage:
        if request.is_ajax():
            # If the request is AJAX and the page is out of range
            # return an empty page
            return HttpResponse('')
        # If page is out of range deliver last page of results
        articles = paginator.page(paginator.num_pages)
    if request.is_ajax():
        return render(request,
                        'articles/article_list_ajax.html',
                        {'section': 'articles', 'articles': articles})
    return render(request,
                    'articles/article_list.html',
                    {'section': 'articles', 'articles': articles})


