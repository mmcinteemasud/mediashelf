from django.conf.urls import url,patterns
from . import views
from wkhtmltopdf.views import PDFTemplateView


urlpatterns = [
	url(r'^article/$', views.article_save, name='article'),
	url(r'^mobile-save/$', views.mobile_save, name='mobile-save'),
	url(r'^detail/(?P<id>\d+)/(?P<slug>[-\w]+)/$',
        views.article_detail, name='detail'),
	url(r'^$', views.article_list, name='list'),
    url(r'^article-delete/(?P<article_id>\d+)/$', views.article_delete, name='article_delete'),
    url(r'^article-edit/(?P<article_id>\d+)/$', views.article_edit, name='article_edit'),
    url(r'^search/$', views.search, name='article-search'),
	url(r'^pdf/(?P<article_id>\d+)/$', views.generate_pdf, name='pdf'),
]


