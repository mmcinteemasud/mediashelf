from django.contrib.auth.decorators import login_required
from common.decorators import ajax_required
from common.search import *
import json
from django.http import JsonResponse
from .models import Video
from urllib.request import urlopen
from urllib.parse import urlparse
from django.shortcuts import render, redirect
from urllib import request
from django.core.files.base import ContentFile
import urllib.request
from actions.utils import create_action
from videos.serializers import VideoSerializer
from rest_framework import generics
from rest_framework.response import Response
from rest_framework import status
from rest_framework.authentication import TokenAuthentication
from rest_framework.permissions import IsAuthenticated
from django.utils import timezone
from django.views.decorators.csrf import csrf_exempt
from pyquery import PyQuery as pq
from PIL import Image
from lxml import etree
from io import BytesIO
import requests
import random
import hashlib
# Create your views here.

@csrf_exempt
def video_save(request):
	if str(request.user) == 'AnonymousUser':
		response_data = {"status":"login"}
		return JsonResponse(response_data)
	else:
		data = (json.loads(request.read().decode('utf-8')))
		user = request.user
		title = data[0]['title']
		category = data[0]['category']
		url = data[0]['url']
		image_url = data[0]['image_url']
		version = hashlib.sha1((str(random.random())).encode('utf8')).hexdigest()[:5]
		video_instance= Video.objects.create(user=user, title=title, category=category, url=url, version=version)
		create_action(user, 'Saved: ' + title, 'Video', video_instance)
		class AppURLopener(urllib.request.FancyURLopener):
			version = "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/35.0.1916.47 Safari/537.36"
		opener = AppURLopener()
		name = str(timezone.now().microsecond) + urlparse(image_url).path.split('/')[-1]
		if image_url != "":
			response = opener.open(image_url)
			image_content = ContentFile(response.read())
			video_instance.image.save(name, image_content)

		return JsonResponse({'status':'Saved successfully!'})

@csrf_exempt
@login_required
def mobile_save(request):
	if str(request.user) == 'AnonymousUser':
		return JsonResponse({"status":"login"})
	else:
		#print('begin')
		data = (json.loads(request.read().decode('utf-8')))
		user = request.user
		#print(user)
		this_url = data[0]['url']
		category=data[0]['category']
		site_url = urlparse(this_url)
		site_name = site_url.netloc
		video_images = []
		d = pq(url=this_url)
		title=d('title').text()
		image_list = d('img')
		def ValidateUrl(test_url):
			global valid_url
			site_url = urlparse(this_url)
			o = urlparse(test_url)
			if bool(o.scheme) == True and o.scheme[0:2] == 'ht':
				valid_url=test_url
			elif test_url[0:2] != 'ht':
				if str(test_url)[0:2] == '//':
					valid_url = site_url.scheme + ':' + test_url
				elif str(test_url)[0:1] == '/':
					valid_url = site_url.scheme + '://' + site_url.netloc + test_url
				else:
					pass
		if 'youtube' in site_name:
			vid_id = this_url.split('=')
			vid_id = vid_id[-1]
			vid_img_url="http://img.youtube.com/vi/" + vid_id + "/hqdefault.jpg"
			video_images.append(vid_img_url)
		elif 'vimeo' in site_name:
			#print('vimeo')
			vid_id = this_url.split('/')
			vid_id = vid_id[-1]
			thumb_url = 'https://vimeo.com/api/v2/video/' + vid_id + '.json'
			r = requests.get(thumb_url)
			data = json.loads(r.text)
			vid_img_url=data[0]['thumbnail_large']
			video_images.append(vid_img_url)
		else:
			for each in image_list:
				current_url = d(each).attr('src')
				source = current_url.split('.')
				if source[-1] == 'gif':
					pass
				else:
					ValidateUrl(current_url)
					if d(each).width() and (int(d(each).width()) >= 95):
						video_images.append(valid_url)
					elif d(each).width() == None:
						response = requests.get(valid_url)
						img = Image.open(BytesIO(response.content))
						width, height = img.size
						if width >= 95:
							video_images.append(valid_url)

		if not video_images:
			image_url = ''
		else:
			image_url = video_images[0]
		version = hashlib.sha1((str(random.random())).encode('utf8')).hexdigest()[:5]
		video_instance= Video.objects.create(user=user, title=title, category=category, url=this_url, version=version)
		create_action(user, 'Saved: ' + title, 'Video', video_instance)
		class AppURLopener(urllib.request.FancyURLopener):
			version = "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/35.0.1916.47 Safari/537.36"
		opener = AppURLopener()
		name = str(timezone.now().microsecond) + urlparse(image_url).path.split('/')[-1]
		if image_url != "":
			response = opener.open(image_url)
			image_content = ContentFile(response.read())
			video_instance.image.save(name, image_content)

		return JsonResponse({'status':'Saved successfully!'})


@login_required
def video_list(request):
    user = request.user
    videos = Video.objects.filter(user= user)
    categories = Video.objects.filter(user=user).values_list('category', flat=True).distinct().exclude(category__isnull=True).exclude(category__exact=' ').order_by('category')
    if request.is_ajax():
        return render(request,
                        'videos/responsive_video_list.html',
                        {'section': 'videos', 'videos': videos})
    return render(request,
                    'videos/video_list.html',
                    {'section': 'videos', 'videos': videos, 'categories': categories})
@ajax_required
@login_required
def video_delete(request, video_id):
	video = Video.objects.get(pk=video_id)
	create_action(request.user, 'Deleted: ' + video.title, 'Video', video)
	video.delete()
	return JsonResponse({'status':'Deleted successfully!'})


@ajax_required
@login_required
def video_edit(request, video_id):
	print('ok')
	print(video_id)
	data = json.loads(request.body.decode("utf-8"))
	print(data[0]['title'])
	user = request.user
	title = data[0]['title']
	category = data[0]['category']
	version = hashlib.sha1((str(random.random())).encode('utf8')).hexdigest()[:5]
	video = Video.objects.get(pk=video_id)
	video.title=title
	video.category=category
	video.version=version
	video.save()
	return JsonResponse({'status':'Success!'})


@login_required
def search(request):
	user=request.user
	query_string = ''
	videos = None
	categories = None
	if ('q' in request.GET) and request.GET['q'].strip():
		query_string = request.GET['q']
		title_entry_query = get_query(query_string, ['title', ])
		videos = Video.objects.filter(user=user).filter(title_entry_query)
		categories = Video.objects.filter(user=user).filter(title_entry_query).values_list('category', flat=True).distinct().exclude(category__isnull=True).exclude(category__exact=' ').order_by('category')
	return render(request,'videos/video_list.html',{'section': 'videos', 'videos': videos, 'categories': categories})


class VideoList(generics.ListCreateAPIView):
    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)
    serializer_class = VideoSerializer
    def get_queryset(self):
        user = self.request.user
        return Video.objects.filter(user=user)

class VideoDetail(generics.RetrieveUpdateDestroyAPIView):
    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)
    serializer_class = VideoSerializer
    def get_queryset(self):
        user = self.request.user
        return Video.objects.filter(user=user)
