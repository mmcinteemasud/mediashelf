from django.conf import settings
from storages.backends.s3boto import S3BotoStorage

class MediaStorage(S3BotoStorage):
    def __init__(self, *args, **kwargs):
        kwargs['bucket'] = getattr(settings, 'AWS_MEDIA_BUCKET_NAME')
        super(MediaStorage, self).__init__(*args, **kwargs)
