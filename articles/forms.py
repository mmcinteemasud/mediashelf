from django import forms
from .models import Image
from urllib import request
from django.core.files.base import ContentFile
from django.utils.text import slugify


class ImageSaveForm(forms.ModelForm):
    class Meta:
        model = Image
        fields = ('image_url',)

    def save(self, force_insert=False,
                    force_update=False,
                    commit=True):
        image = super(ImageSaveForm, self).save(commit=False)
        image_url = self.cleaned_data['image_url']
        image_name = '{}.{}'.format(
                                image_url.rsplit('.', 1)[1].lower())
        # download image from the given URL
        response = request.urlopen(image_url)
        image.image.save(image_name,
                        ContentFile(response.read()),
                        save=True)
        image.save()
        
