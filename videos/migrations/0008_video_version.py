# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('videos', '0007_auto_20170510_1700'),
    ]

    operations = [
        migrations.AddField(
            model_name='video',
            name='version',
            field=models.CharField(max_length=40, default=1111),
            preserve_default=False,
        ),
    ]
