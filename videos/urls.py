from django.conf.urls import url
from . import views


urlpatterns = [
	url(r'^video/$', views.video_save, name='video_save'),
	url(r'^mobile-save/$', views.mobile_save, name='mobile-save'),
	url(r'^$', views.video_list, name='list'),
    url(r'^video-delete/(?P<video_id>\d+)/$', views.video_delete, name='video_delete'),
    url(r'^video-edit/(?P<video_id>\d+)/$', views.video_edit, name='video_edit'),
    url(r'^search/$', views.search, name='video-search'),
]
