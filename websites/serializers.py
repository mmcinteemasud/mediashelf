from rest_framework import serializers
from websites.models import Website


class WebsiteSerializer(serializers.ModelSerializer):
    class Meta:
        model = Website
        fields = ('id', 'site_name', 'url', 'category', 'favicon', 'created', 'version')
