# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('websites', '0004_auto_20170429_2156'),
    ]

    operations = [
        migrations.AlterField(
            model_name='website',
            name='favicon',
            field=models.ImageField(upload_to='website_favicons/%Y/%m/%d', blank=True, default='website_favicons/internet.png'),
        ),
    ]
