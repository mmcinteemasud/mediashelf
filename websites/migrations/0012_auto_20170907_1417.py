# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('websites', '0011_auto_20170907_1311'),
    ]

    operations = [
        migrations.AlterField(
            model_name='website',
            name='created',
            field=models.DateTimeField(db_index=True, auto_now_add=True),
        ),
    ]
