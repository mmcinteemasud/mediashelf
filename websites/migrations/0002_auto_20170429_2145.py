# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('websites', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='website',
            name='favicon',
            field=models.ImageField(default='website_images/internet.ico', upload_to='website_favicons/%Y/%m/%d'),
        ),
    ]
