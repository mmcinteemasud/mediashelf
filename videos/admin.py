from django.contrib import admin
from .models import Video

# Register your models here.


class VideoAdmin(admin.ModelAdmin):
    list_display = ['title', 'category', 'url', 'saved_on']
    list_filter = ['saved_on']
admin.site.register(Video, VideoAdmin)
