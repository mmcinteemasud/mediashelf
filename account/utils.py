from django.shortcuts import redirect
from django.contrib.auth.models import User
from social_core.pipeline.partial import partial
from .models import Profile

# partial says "we may interrupt, but we will come back here again"

    # Pipeline method
@partial

def create_profile(strategy, details, response, user, *args, **kwargs):

    if Profile.objects.filter(user=user).exists():
        pass
    else:
        new_profile = Profile(user=user)
        new_profile.save()

    return kwargs
