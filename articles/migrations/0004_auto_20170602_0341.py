# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('articles', '0003_auto_20170517_2113'),
    ]

    operations = [
        migrations.AlterField(
            model_name='article',
            name='url',
            field=models.URLField(max_length=500),
        ),
        migrations.AlterField(
            model_name='image',
            name='image_url',
            field=models.TextField(max_length=500),
        ),
    ]
