(function(){
  var jquery_version = '2.1.4';
  var site_url = 'http://mysite.com:8000/';
  var static_url = site_url + 'static/';
  var min_width = 100;
  var min_height = 100;

  function bookmarklet(msg) {
    // load CSS
    var css = jQuery('<link>');
	
    css.attr({
      rel: 'stylesheet',
      type: 'text/css',
      href: static_url + 'css/bookmarklet.css?r=' + Math.floor(Math.random()*99999999999999999999)
    
});
    jQuery('head').append(css);
	jQuery('head').append('<link rel="stylesheet" href="http://mysite.com:8000/static/css/font-awesome-4.7.0/css/font-awesome.min.css" type="text/css" />');
	

    // load HTML
article_categories_html='<div class="article_categories"> <h1>Choose a category:</h1><fieldset id="fieldset"><div class="item"><input type = "radio" name="action" id = "chkArts" value = "Arts" /><label for = "arts">Arts</label></div><div class="item"><input type = "radio"name="action"id = "chkBusiness"value = "Business" /><label for = "chkBusiness">Business</label></div><div class="item"><input type = "radio"name="action"id = "chkEntertainment"value = "Entertainment" /><label for = "chkEntertainment">Entertainment</label></div><div class="item"><input type = "radio"name="action"id = "chkEducation"value = "Education" /><label for = "chkEducation">Education</label></div><div class="item"><input type = "radio"name="action"id = "chkFood"value = "Food" /><label for = "chkFood">Food</label></div><div class="item"><input type = "radio"name="action"id = "chkHealth"value = "Health" /><label for = "chkHealth">Health</label></div><div class="item"><input type = "radio" name="action"id = "chkHistory"value = "History" /><label for = "chkHistory">History</label></div><div class="item"><input type = "radio"name="action"id = "chkPolitics"value = "Politics" /><label for = "chkPolitics">Politics</label></div><div class="item"><input type = "radio"name="action"id = "chkScience"value = "Science" /><label for = "chkScience">Science</label></div><div class="item"><input type = "radio"name="action"id = "chkSociety"value = "Society" /><label for = "chkSociety">Society</label></div><div class="item"><input type = "radio"name="action"id = "chkSports"value = "Sports" /><label for = "chkSports">Sports</label></div><div class="item"><input type = "radio"name="action"id = "chkTechnology"value = "Technology" /><label for = "chkTechnology">Technology</label></div><div class="item"><input type = "radio"name="action"id = "chkTravel"value = "Travel" /><label for = "chkTravel">Travel</label></div><div class="item"><input type = "radio"name="action"id = "chkMiscellaneous"value = "Miscellaneous" /><label for = "chkMiscellaneous">Miscellaneous</label></div></fieldset><button type = "button"class="bookmarklet-button"id = "save_article">Save Article</button></div>';

    box_html = '<div id="bookmarklet"><div id="header"><div class="logo"></div></div><div id="container"><div id="images"></div><div id="article_categories"></div><div id="video_categories"></div><div id="website_categories"></div><div id="status_display"></div><div id="get_buttons"><p>What would you like to save?</p><span class="bookmarklet-button"><button id="image_button">Images</button></span><span class="bookmarklet-button"><button id="article_button">Article</button></span><span class="bookmarklet-button"><button id="video_button">Video</button></span><span class="bookmarklet-button"><button id="website_button">WebSite</button></span></div><br clear="all" /></div><i id="close" class="fa fa-window-close"></i></div>';
    jQuery('body').append(box_html);

	  // close event
	  jQuery('#bookmarklet #close').click(function(){
      jQuery('#bookmarklet').remove();
	  });

// Send a message containing the page details back to the event page


article = [];
video = [];
website = [];
var get_video;
var img_url;
var favicon;

//validate and clean url
function validUrl(test_url) {
    if (test_url) {
        if (/^(http|https|ftp):\/\/[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$/i.test(test_url)) {
            return true;
            //alert("valid URL");
            //console.log(test_url);

        } 

		if (test_url.substring(0,2)=='//'){
			img_url = location.protocol + test_url;
		
		}
		else 
if (test_url.substring(0,1)=='/'){
			img_url = location.protocol + '//' + location.host + test_url;
		
		}
else {
            img_url = location.protocol + '//' + location.host + '/' + test_url;
            //alert("invalid URL");
            //console.log(img_url);
        }
    } else {
        img_url = "";
    }
}

//validate and clean favicon url
function validFaviconUrl(test_url) {
    if (test_url) {
        if (/^(http|https|ftp):\/\/[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$/i.test(test_url)) {
            return true;
            //alert("valid URL");
            //console.log(test_url);

        } 

		if (test_url.substring(0,2)=='//'){
			favicon = location.protocol + test_url;
		
		}
		else 
if (test_url.substring(0,1)=='/'){
			favicon = location.protocol + '//' + location.host + test_url;
		
		}
else {
            favicon = location.protocol + '//' + location.host + '/' + test_url;
            //alert("invalid URL");
            //console.log(img_url);
        }
    } else {
        favicon = "";
    }
}
//get favicon 

var getFavicon = function(){
    var nodeList = document.getElementsByTagName("link");
    for (var i = 0; i < nodeList.length; i++)
    {
        if((nodeList[i].getAttribute("rel") == "icon")||(nodeList[i].getAttribute("rel") == "shortcut icon"))
        {
            favicon = nodeList[i].getAttribute("href");
        }
    }
    return favicon;        
}
getFavicon();
validFaviconUrl(favicon);


//get article, image, site info
var title = $(document).attr('title');
$('body').find("img:hidden").show();
//var get_article = $('body').find(":hidden").remove();


var get_article = $(get_article).ready(function() {

    $('img').each(function() {
		console.log(this);
        if ($(this).width() < 95 || $(this).height() < 121) {
            $(this).detach();
        }
    });

    $('p').each(function() {
        if ($(this).is(':contains("Advertisement")')) {
            $(this).detach();
        }

        if ($(this).is(':contains("ADVERTISEMENT")')) {
            $(this).detach();
        }

    });
    return get_article;
});
vid_img_url = $('body').find('img[src$=".jpg"]:first').attr("src");
var get_article = $('p, img');




var header_items = {
    title: title,
    category: " ",
    url: $(location).attr('href'),
	favicon_url: favicon
};
article.push(header_items);


for (i = 0; i < get_article.length; i++) {
    if ($(get_article[i]).is('p')) {
        var p_items = {
            node: i,
            html_element: "p",
            content: $(get_article[i]).text()
        };
        article.push(p_items);
    }
    if ($(get_article[i]).is('img')) {
        if ($(get_article[i]).attr("src")) {
          if (/^(http|https|ftp):\/\/[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$/i.test($(get_article[i]).attr("src"))) {
            var img_items = {
                node: i,
                html_element: "img",
                content: $(get_article[i]).attr("src")
            };
            article.push(img_items);

          }
}
         else if ($(get_article[i]).attr("srcset")) {
            if (/^(http|https|ftp):\/\/[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$/i.test($(get_article[i]).attr("srcset"))) {
              var img_items = {
                  node: i,
                  html_element: "img",
                  content: $(get_article[i]).attr("srcset").split(' ')[0]
              };
              article.push(img_items);
        }
    }
}

if (window.location.href.indexOf("youtube") != -1) {
    console.log("this is youtube");
    var regExp = /.*(?:youtu.be\/|v\/|u\/\w\/|embed\/|watch\?v=)([^#\&\?]*).*/;
    var match = window.location.href.match(regExp);
    if (match && match[1].length == 11) {
        urllink = match[1];
        vid_img_url = "http:\/\/img.youtube.com\/vi\/" + urllink + "\/hqdefault.jpg";
    }
} else if (window.location.href.indexOf("vimeo.com") != -1) {
    console.log("this is vimeo");
    var regExp = /.*(www\.)?vimeo.com\/(\d+)($|\/)/;
    var match = window.location.href.match(regExp);
    if (match) {
        video_id = match[2];
        var getVimeoThumbnail = function(id) {
            $.ajax({
                type: 'GET',
                url: 'https://vimeo.com/api/v2/video/' + id + '.json',
                jsonp: 'callback',
                dataType: 'jsonp',
                async: false,
                success: function(data) {
                    vid_img_url = (data[0].thumbnail_large);
                }
            });
        };
        getVimeoThumbnail(video_id);
    }
} 

else {
    vid_img_url = img_url;
}
}

//validate and clean vid_img_url
validUrl(img_url);

var get_video = {
    title: $('title').text(),
    url: $(location).attr('href'),
    category: " ",
    image_url: img_url

};
video.push(get_video);
var images = $(get_article).filter('img').map(function() {
    return this.src;
}).toArray();

var get_website = {
	site_name: location.host,
	url: $(location).attr('href'),
	category: " ",
	favicon_url: favicon
};
website.push(get_website);
console.log(website[0]);
function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie !== '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) === (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}


console.log(article);



  
  var checkImageButton = document.getElementById('image_button');
  var checkVideoButton = document.getElementById('video_button');
  var checkWebsiteButton = document.getElementById('website_button');

  var saveArticleButton = document.getElementById("save_article");
  var saveVideoButton = document.getElementById("save_video");
  var saveWebsiteButton = document.getElementById("save_website");


//When article button is clicked, this will retrieve the article
$(".bookmarklet-button #article_button").on('click', function() {
	$('#bookmarklet #get_buttons').hide();
	$("#bookmarklet #article_categories").append(article_categories_html);
	var category_array = new Array();
//When 'save' button is clicked, this will set csrf headers and send the article
$(document).on("click", ".item input[name='action']", function add_category(event) {
	category_array.push($('input[name=action]:checked').val());
});
function article_save(cat_append){
    window.setTimeout(function() {($("#bookmarklet #article_categories").hide());}, 50);
    var statusDisplay = null;
    var site_url = 'http://mysite.com:8000/';
    var postUrl = site_url +'articles/article/';
	//var redirectUrl = site_url +'articles/';
    var params = cat_append;
	var csrftoken = $cookies.get('csrftoken');
	console.log(csrftoken);

    // when the article is desired open URL with it

    var xhr = new XMLHttpRequest();
    xhr.open('POST', postUrl, true);
    xhr.withCredentials = true;
    //xhr.setRequestHeader("X-CSRFTOKEN", csrftoken);
	xhr.setRequestHeader('Content-type', "application/json; charset=utf-8");
        xhr.onreadystatechange=function() {
            //window.close();
            if (xhr.readyState == 4) {
                console.log('Saved successfuly!1');

                            if (xhr.status == 200) {
									console.log(xhr.responseText);
                                  var data = JSON.parse(xhr.responseText);
                // If it was a success, close the popup after a short delay
                $('body').show();
                $('#status_display').append("<p>" + data["status"] + "</p>").attr('style', 'text-align: center');
                window.setTimeout(function() {(window.close());}, 3000);
                //$("#status_display").load("status_display.html");
                console.log(data);


            }

		}
};

      // open new window to submit the image
    xhr.send(JSON.stringify(params));
    window.setTimeout(function() {($('#bookmarklet').remove());}, 51);
    //window.setTimeout($('body').hide(), 1000);





};
//When the 'save' button is clicked, this will append the user selected category to the article and invoke the article_save function to send the article to the backend
$(document).on("click", "#save_article", function() {
	console.log(category_array[category_array.length-1]);
	article[0]['category'] = category_array[category_array.length-1];
 	var cat_appended = article;
	console.log(cat_appended);
$(document).on("click", "#save_article", article_save(cat_appended));
});


});
//When video button is clicked, this will retrieve the video
$(checkVideoButton).on('click', function() {
	document.getElementById("get_buttons").style.display = 'none';
	$("#video_categories").load("video_categories.html");
	var category_array = new Array();
//When 'save' button is clicked, this will set csrf headers and send the video
$(document).on("click", ".item input[name='action']", function add_category(event) {
	category_array.push($('input[name=action]:checked').val());
});
function video_save(cat_append){
  window.setTimeout(function() {(document.getElementById("video_categories").style.display = 'none');}, 50)
    var statusDisplay = null;
    var site_url = 'http://mysite.com:8000/';
    var postUrl = site_url +'videos/video/';
	var redirectUrl = site_url +'videos/';
    var params = cat_append;

getCSRF(postUrl, "csrftoken",
    // when the video is desired open URL with it
function(csrftoken){
    var xhr = new XMLHttpRequest();
    xhr.open('POST', postUrl, true);
    xhr.withCredentials = true;
    xhr.setRequestHeader("X-CSRFTOKEN", csrftoken);
	xhr.setRequestHeader('Content-type', "application/json; charset=utf-8");
        xhr.onreadystatechange=function() {
            if (xhr.readyState == 4) {
                console.log('Saved successfuly!');
                            if (xhr.status == 200) {
                              var data = JSON.parse(xhr.responseText);
                // If it was a success, close the popup after a short delay
                $('body').show();
                $('#status_display').append("<p>" + data["status"] + "</p>").attr('style', 'text-align: center');
                window.setTimeout(function() {(window.close());}, 3000);

            }

		}
};

      // open new window to submit the image
    xhr.send(JSON.stringify(params));
    window.setTimeout(function() {($('body').hide());}, 51);
    //window.setTimeout(window.open(postUrl), 1000);




});
};
//When the 'save' button is clicked, this will append the user selected category to the video and invoke the video_save function to send the video to the backend
$(document).on("click", "#save_video", function() {
	console.log(category_array[category_array.length-1]);
	video[0]['category'] = category_array[category_array.length-1];
 	var cat_appended = video;
	console.log(cat_appended);
$(document).on("click", "#save_video", video_save(cat_appended));
});


});


//When website button is clicked, this will retrieve the website category list
$(checkWebsiteButton).on('click', function() {
	document.getElementById("get_buttons").style.display = 'none';
	$("#website_categories").load("website_categories.html");
	var category_array = new Array();
//When 'save' button is clicked, this will set csrf headers and send the website info
$(document).on("click", ".item input[name='action']", function add_category(event) {
	category_array.push($('input[name=action]:checked').val());
});
function website_save(cat_append){
  window.setTimeout(function() {(document.getElementById("website_categories").style.display = 'none');}, 50)
    var statusDisplay = null;
    var site_url = 'http://mysite.com:8000/';
    var postUrl = site_url +'websites/website/';
	var redirectUrl = site_url +'websites/';
    var params = cat_append;

getCSRF(postUrl, "csrftoken",
    // when the video is desired open URL with it
function(csrftoken){
    var xhr = new XMLHttpRequest();
    xhr.open('POST', postUrl, true);
    xhr.withCredentials = true;
    xhr.setRequestHeader("X-CSRFTOKEN", csrftoken);
	xhr.setRequestHeader('Content-type', "application/json; charset=utf-8");
        xhr.onreadystatechange=function() {
            if (xhr.readyState == 4) {
                console.log('Saved successfuly!');
                            if (xhr.status == 200) {
                              var data = JSON.parse(xhr.responseText);
                // If it was a success, close the popup after a short delay
                $('body').show();
                $('#status_display').append("<p>" + data["status"] + "</p>").attr('style', 'text-align: center');
                window.setTimeout(function() {(window.close());}, 3000);

            }

		}
};

      // open new window to submit the image
    xhr.send(JSON.stringify(params));
    window.setTimeout(function() {($('body').hide());}, 51);
    //window.setTimeout(window.open(postUrl), 1000);




});
};
//When the 'save' button is clicked, this will append the user selected category to the website and invoke the website_save function to send the website to the backend
$(document).on("click", "#save_website", function() {
	console.log(category_array[category_array.length-1]);
	website[0]['category'] = category_array[category_array.length-1];
 	var cat_appended = website;
	console.log(cat_appended);
$(document).on("click", "#save_website", website_save(cat_appended));
});


});
//When image button is clicked, this will retrieve the images

  $(checkImageButton).on('click', function() {
    $('#images').append('<p>Choose an image to save:</p>');
for(var i = 0; i < pageDetails.images.length; i++){
	document.getElementById("get_buttons").style.display = 'none';
	var images = pageDetails.images[i];
    $('#images').append('<a href="#"><img src="'+ images +'" /></a>');
};
$('#images a').on('click', image_save);
  }, false);



// Global reference to the status display SPAN
var statusDisplay = null;
var site_url = 'http://mysite.com:8000/';
    // when an image is selected open URL with it
function image_save(){
      selected_image = jQuery(this).children('img').attr('src');
      // hide bookmarklet
      window.setTimeout(window.close, 1000);
      // open new window to submit the image
      window.open(site_url +'images/create/?url='
                  + encodeURIComponent(selected_image)
                  + '&title=' + encodeURIComponent(jQuery('title').text()),
                  '_blank');
    };

 };
  // Check if jQuery is loaded
  if(typeof window.jQuery != 'undefined') {
    bookmarklet();
  } else {
    // Check for conflicts
    var conflict = typeof window.$ != 'undefined';
    // Create the script and point to Google API
    var script = document.createElement('script');
    script.setAttribute('src','http://ajax.googleapis.com/ajax/libs/jquery/' + jquery_version + '/jquery.min.js');
    // Add the script to the 'head' for processing
    document.getElementsByTagName('head')[0].appendChild(script);
    // Create a way to wait until script loading
    var attempts = 15;
    (function(){
      // Check again if jQuery is undefined
      if(typeof window.jQuery == 'undefined') {
        if(--attempts > 0) {
          // Calls himself in a few milliseconds
          window.setTimeout(arguments.callee, 250)
        } else {
          // Too much attempts to load, send error
          alert('An error ocurred while loading jQuery')
        }
      } else {
          bookmarklet();
      }
    })();
  }

})()
