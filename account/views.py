from django.shortcuts import render
from django.http import HttpResponse
from django.contrib.auth import authenticate, login
from .forms import LoginForm
from django.contrib.auth.decorators import login_required
from .models import Profile
from images.models import Image
from articles.models import Article
from videos.models import Video
from .forms import LoginForm, UserRegistrationForm, UserEditForm, ProfileEditForm
from django.contrib import messages
from social.backends.utils import load_backends
from django.shortcuts import get_object_or_404
from django.utils import timezone
import random
import hashlib
import datetime
from actions.models import Action
from actions.utils import create_action
from .tasks import EmailVerification
from django.contrib.auth.models import User

@login_required
def edit(request):
    if request.method == 'POST':
        user_form = UserEditForm(instance=request.user,
                                 data=request.POST)
        profile_form = ProfileEditForm(
                                    instance=request.user.profile,
                                    data=request.POST,
                                    files=request.FILES)
        if user_form.is_valid() and profile_form.is_valid():
            user_form.save()
            profile_form.save()
            messages.success(request, 'Profile updated '\
                             'successfully')
        else:
            messages.error(request, 'Error updating your profile')

    else:
        user_form = UserEditForm(instance=request.user)
        profile_form = ProfileEditForm(instance=request.user.profile)
    return render(request, 'account/edit.html', {'user_form': user_form,
                                                 'profile_form': profile_form})


@login_required
def dashboard(request):
    user = request.user
    random_articles = None
    random_videos = None
    random_images = None
    actions = Action.objects.filter(user=user)\
        .prefetch_related('target')
    actions = actions[:10]

    article_ids = list(Article.objects.values_list('id', flat=True).filter(
        user=user))
    video_ids = list(Video.objects.values_list('id', flat=True).filter(
        user=user))
    image_ids = list(Image.objects.values_list('id', flat=True).filter(
        user=user))
    n = 3

    if article_ids and len(article_ids) > 2:
        rand_article_ids = random.sample(article_ids, n)
        random_articles = Article.objects.filter(id__in=rand_article_ids)
    if video_ids and len(video_ids) > 2:
        rand_video_ids = random.sample(video_ids, n)
        random_videos = Video.objects.filter(id__in=rand_video_ids)
    if image_ids and len(image_ids) > 2:
        rand_image_ids = random.sample(image_ids, n)
        random_images = Image.objects.filter(id__in=rand_image_ids)

    return render(request,
                  'account/dashboard.html',
                  {'section': 'dashboard',
                   'articles': random_articles,
                   'videos': random_videos,
                   'images': random_images,
                   'actions': actions})

def user_login(request):
    if request.method=='POST':
        form = LoginForm(request.POST)
        if form.is_valid():
            cd = form.cleaned_data
            user = authenticate(username=cd['username'],
                               password=cd['password'])
            if user is not None:
                if user.is_active:
                    login(request, user)
                    return HttpResponse('Authenticated '\
                                        'successfully')
                else:
                    return HttpResponse('Disabled account')
            else:
                return HttpResponse('Invalid login')
    else:
        form = LoginForm()
    return render(request, 'account/login.html', {'form': form})

def register(request):
    if request.method == 'POST':
        user_form = UserRegistrationForm(request.POST)
        if user_form.is_valid():
            # Create a new user object but avoid saving it yet
            new_user = user_form.save(commit=False)
            # Set the chosen password
            new_user.set_password(
                user_form.cleaned_data['password'])
            # Save the User object
            new_user.is_active=False
            new_user.save()
            create_action(new_user, 'By: ' + new_user.username, 'Account created', new_user)
            #We generate a random activation key
            salt = hashlib.sha1((str(random.random())).encode('utf8')).hexdigest()[:5]
            usernamesalt = new_user.username
            if isinstance(usernamesalt, str):
                usernamesalt = usernamesalt.encode('utf8')
                usernamesalt = usernamesalt.decode('utf8')

            # Create the user profile
            profile = Profile(user=new_user)
            profile.activation_key= hashlib.sha1((salt+usernamesalt).encode('utf8)')).hexdigest()
            profile.key_expires=str(timezone.localtime(timezone.now()) + timezone.timedelta(days=2))

            profile.save()
            EmailVerification.delay(user_id=new_user.pk)
            return render(request,
                            'account/verification_sent.html',
                            {'new_user': new_user})
    else:
        user_form = UserRegistrationForm()
    return render(request,
                    'account/register.html',
                    {'user_form': user_form})
    # View that handles /already-registered:
#View called from activation email. Activate user if link didn't expire (48h default), or offer to
#send a second link if the first expired.
def activation(request, key):
    activation_expired = False
    already_active = False
    profile = get_object_or_404(Profile, activation_key=key)
    if profile.user.is_active == False:
        if str(timezone.localtime(timezone.now())) > str(profile.key_expires):
            activation_expired = True #Display: offer the user to send a new activation link
            id_user = profile.user.id
            return render(request,
                    'account/new_activation_link.html',
                    {'id_user': id_user})
        else: #Activation successful
            profile.user.is_active = True
            profile.user.save()
    #If user is already active, simply display error message
    else:
        already_active = True #Display : error message
        return render (request, 'already_active.html',
                        {'error': request.GET.get('err')})

    return render(request, 'register_done.html')


def new_activation_link(request, user_id):
    user = User.objects.get(id=user_id)
    profile = user.profile
    if str(timezone.localtime(timezone.now())) < str(profile.key_expires):
        return render(request,
                    'already_sent.html')
    else:
        if user is not None and not user.is_active:
            username=user.username
            salt = hashlib.sha1((str(random.random())).encode('utf8')).hexdigest()[:5]
            usernamesalt = username
            if isinstance(usernamesalt, str):
                usernamesalt = usernamesalt.encode('utf8')
                usernamesalt = usernamesalt.decode('utf8')

            # Create the user profile
            profile.activation_key= hashlib.sha1((salt+usernamesalt).encode('utf8)')).hexdigest()
            profile.key_expires=str(timezone.localtime(timezone.now()) + timezone.timedelta(days=2))

            profile.save()

            EmailVerification.delay(user_id=user.pk)
            return render(request,
                            'account/new_verification_sent.html',
                            {'user': user})

        else:
            return render (request, 'already_active.html',
                              {'error': request.GET.get('err')})


def already_registered(request):
    return render (request, 'already_registered.html',
                              {'error': request.GET.get('err')})
