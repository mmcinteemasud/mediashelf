# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('websites', '0008_auto_20170510_1700'),
    ]

    operations = [
        migrations.AddField(
            model_name='website',
            name='version',
            field=models.CharField(max_length=40, default=1111),
            preserve_default=False,
        ),
    ]
