from django.conf.urls import url
from rest_framework.urlpatterns import format_suffix_patterns
from articles import views as article_views
from images import views as image_views
from videos import views as video_views
from websites import views as website_views
from rest_framework.authtoken import views as api_views

urlpatterns = [
    url(r'^articles/$', article_views.ArticleList.as_view()),
    url(r'^articles/(?P<pk>[0-9]+)/$', article_views.ArticleDetail.as_view()),
    url(r'^images/$', image_views.ImageList.as_view()),
    url(r'^images/(?P<pk>[0-9]+)/$', image_views.ImageDetail.as_view()),
    url(r'^videos/$', video_views.VideoList.as_view()),
    url(r'^videos/(?P<pk>[0-9]+)/$', video_views.VideoDetail.as_view()),
    url(r'^websites/$', website_views.WebsiteList.as_view()),
    url(r'^websites/(?P<pk>[0-9]+)/$', website_views.WebsiteDetail.as_view()),
]

urlpatterns += [
    url(r'^api-token-auth/', api_views.obtain_auth_token)
]

urlpatterns = format_suffix_patterns(urlpatterns)
