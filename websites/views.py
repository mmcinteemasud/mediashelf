from django.shortcuts import render
import json
from django.contrib.auth.decorators import login_required
from common.decorators import ajax_required
from common.search import *
from django.http import JsonResponse
from actions.utils import create_action
from .models import Website
from django.core.files.base import ContentFile
import urllib.request
from urllib.parse import urlparse
from django.utils import timezone
from websites.serializers import WebsiteSerializer
from rest_framework import generics
from rest_framework.response import Response
from rest_framework import status
from rest_framework.authentication import TokenAuthentication
from rest_framework.permissions import IsAuthenticated
from django.views.decorators.csrf import csrf_exempt
from pyquery import PyQuery as pq
from lxml import etree
import random
import hashlib

# Create your views here.

@csrf_exempt
def websiteSave(request):
	if str(request.user) == 'AnonymousUser':
		response_data = {"status":"login"}
		return JsonResponse(response_data)
	else:
		data = (json.loads(request.read().decode('utf-8')))
		user = request.user
		site_name = data[0]['site_name']
		if site_name[0:4] == 'www.':
			site_name = site_name[4:]
		url = data[0]['url']
		category = data[0]['category']
		favicon_url=data[0]['favicon_url']
		version = hashlib.sha1((str(random.random())).encode('utf8')).hexdigest()[:5]
		website_instance = Website.objects.create(user=user, site_name=site_name, url=url, category=category, favicon_url=favicon_url, version=version)
		class AppURLopener(urllib.request.FancyURLopener):
			version = "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/35.0.1916.47 Safari/537.36"
		opener = AppURLopener()
		name = str(timezone.now().microsecond) + urlparse(favicon_url).path.split('/')[-1]
		if favicon_url != '':
			response = opener.open(favicon_url)
			favicon = ContentFile(response.read())
			website_instance.favicon.save(name, favicon)
		create_action(request.user, 'Saved: ' + site_name, 'Website', website_instance)

		return JsonResponse({'status':'Saved successfully!'})


@csrf_exempt
@login_required
def mobile_save(request):
	if str(request.user) == 'AnonymousUser':
		return JsonResponse({"status":"login"})
	else:
		#print('begin')
		data = (json.loads(request.read().decode('utf-8')))
		user = request.user
		#print(user)
		this_url = data[0]['url']
		category=data[0]['category']
		site_url = urlparse(this_url)
		site_name = site_url.netloc
		if site_name[0:4] == 'www.':
			site_name = site_name[4:]
		d = pq(url=this_url)
		favicon_list = d('link')
		#print('ok1')
		def ValidateUrl(test_url):
			global valid_url
			o = urlparse(test_url)
			if bool(o.scheme) == True and o.scheme[0:2] == 'ht':
				valid_url=test_url
			elif test_url[0:2] != 'ht':
				if str(test_url)[0:2] == '//':
					valid_url = site_url.scheme + ':' + test_url
				elif str(test_url)[0:1] == '/':
					valid_url = site_url.scheme + '://' + site_url.netloc + test_url
				else:
					pass

		for each in favicon_list:
			if d(each).attr('rel') == 'icon' or d(each).attr('rel')== "shortcut icon":
				favicon_url = (d(each).attr('href'))
				ValidateUrl(favicon_url)
			else:
				valid_url = ''
		version = hashlib.sha1((str(random.random())).encode('utf8')).hexdigest()[:5]
		website_instance = Website.objects.create(user=user, site_name=site_name, url=this_url, category=category, favicon_url=favicon_url, version=version)	
		#print('ok2')
		class AppURLopener(urllib.request.FancyURLopener):
			version = "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/35.0.1916.47 Safari/537.36"
		opener = AppURLopener()
		name = str(timezone.now().microsecond) + urlparse(favicon_url).path.split('/')[-1]
		if valid_url != '':
			response = opener.open(valid_url)
			favicon = ContentFile(response.read())
			website_instance.favicon.save(name, favicon)
		#print('4')
		create_action(request.user, 'Saved: ' + site_name, 'Website', website_instance)

		return JsonResponse({'status':'Saved successfully!'})


@login_required
def websiteList(request):
    user = request.user
    websites = Website.objects.filter(user= user)
    categories = Website.objects.filter(user=user).values_list('category', flat=True).distinct().exclude(category__isnull=True).exclude(category__exact=' ').order_by('category')
    if request.is_ajax():
        return render(request,
                        'websites/responsive_website_list.html',
                        {'section': 'websites', 'websites': websites})
    return render(request,
                    'websites/website_list.html',
                    {'section': 'websites', 'websites': websites, 'categories': categories})


@ajax_required
@login_required
def website_delete(request, website_id):
	website = Website.objects.get(pk=website_id)
	create_action(request.user, 'Deleted: ' + website.url, 'website', website)
	website.delete()
	return JsonResponse({'status':'Deleted successfully!'})


@ajax_required
@login_required
def website_edit(request, website_id):
	data = json.loads(request.body.decode("utf-8"))
	user = request.user
	site_name = data[0]['site_name']
	category = data[0]['category']
	version = hashlib.sha1((str(random.random())).encode('utf8')).hexdigest()[:5]
	website = Website.objects.get(pk=website_id)
	website.site_name=site_name
	website.category=category
	website.version=version
	website.save()
	return JsonResponse({'status':'Success!'})


@login_required
def search(request):
	user=request.user
	query_string = ''
	websites = None
	categories = None
	if ('q' in request.GET) and request.GET['q'].strip():
		query_string = request.GET['q']
		title_entry_query = get_query(query_string, ['site_name', ])
		websites = Website.objects.filter(user=user).filter(title_entry_query)
		categories = Website.objects.filter(user=user).filter(title_entry_query).values_list('category', flat=True).distinct().exclude(category__isnull=True).exclude(category__exact=' ').order_by('category')
	return render(request,'websites/website_list.html',{'section': 'websites', 'websites': websites, 'categories': categories})


class WebsiteList(generics.ListCreateAPIView):
    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)
    serializer_class = WebsiteSerializer
    def get_queryset(self):
        user = self.request.user
        return Website.objects.filter(user=user)

class WebsiteDetail(generics.RetrieveUpdateDestroyAPIView):
    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)
    serializer_class = WebsiteSerializer
    def get_queryset(self):
        user = self.request.user
        return Website.objects.filter(user=user)
