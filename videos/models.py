from django.db import models
from django.conf import settings
from django.utils.text import slugify
from django.core.urlresolvers import reverse
# Create your models here.

class Video(models.Model):
	user = models.ForeignKey(settings.AUTH_USER_MODEL,
                            related_name='videos_saved')
	title = models.CharField(max_length=200)
	slug = models.SlugField(max_length=200,
                            blank=True)
	category = models.CharField(max_length=200, default='blank')
	url = models.URLField(max_length=400)
	image_url = models.URLField(max_length=400)
	image = models.ImageField(upload_to='video_images/%Y/%m/%d', default='video_images/default_video_img.jpg', blank=True)
	saved_on = models.DateTimeField(auto_now_add=True)
	version = models.CharField(max_length=40)
	class Meta:
    		ordering = ["-saved_on"]
	def __str__(self):
 		return self.title
    
	def save(self, *args, **kwargs):
		if not self.slug:
			self.slug = slugify(self.title)
			super(Video, self).save(*args, **kwargs)
		else:
			super(Video, self).save(*args, **kwargs)

	#def get_absolute_url(self):
		#return reverse('videos:detail', args=[self.id, self.slug])

