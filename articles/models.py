from django.db import models
from django.conf import settings
from django.utils.text import slugify
from django.core.urlresolvers import reverse
from imagekit.models import ProcessedImageField, ImageSpecField
from imagekit.processors import SmartResize, ResizeToFit
# from django.core.files import File
# import urllib
# from io import BytesIO
# from urllib.request import urlopen
# from urllib.parse import urlparse
# from django.core.files.temp import NamedTemporaryFile
# import os
# from urllib import request
# from django.core.files.base import ContentFile
# Create your models here.


class Article(models.Model):
	user = models.ForeignKey(settings.AUTH_USER_MODEL,
                            related_name='articles_saved')
	title = models.CharField(max_length=200)
	slug = models.SlugField(max_length=200,
                            blank=True)
	category = models.CharField(max_length=200)
	url = models.URLField(max_length=500)
	saved_on = models.DateTimeField(auto_now_add=True)
	version = models.CharField(max_length=40)
	class Meta:
    		ordering = ["-saved_on"]

	def __str__(self):
 		return self.title

	def save(self, *args, **kwargs):
		if not self.slug:
			self.slug = slugify(self.title)
			super(Article, self).save(*args, **kwargs)
		else:
			super(Article, self).save(*args, **kwargs)

	def get_absolute_url(self):
		return reverse('articles:detail', args=[self.id, self.slug])


class Image(models.Model):
	article = models.ForeignKey(
	    Article, on_delete=models.CASCADE, null=True, related_name='image')
	image_url = models.TextField(max_length=500)
	image = ProcessedImageField(upload_to='article_images/%Y/%m/%d', blank=True,
                                           processors=[ResizeToFit(500)],
                                           format='JPEG',
                                           options={'quality': 70})

	def __str__(self):
		return self.image_url
	def get_absolute_url(self):
		return reverse('articles:detail', args=[self.article.id, self.article.slug])

class Node(models.Model):
	article = models.ForeignKey(Article, on_delete=models.CASCADE, null=True, related_name='node')
	#user = models.ForeignKey(settings.AUTH_USER_MODEL,
                            #related_name='user', default=1)
	image = models.ForeignKey(Image, on_delete=models.CASCADE, null=True, blank=True, related_name='node')
	node = models.IntegerField(default=0)
	html_element = models.CharField(max_length=200)
	content = models.TextField(default='blank', max_length=800)

	class Meta:
		ordering = ('node',)
	def __str__(self):
		return self.content
