# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Profile',
            fields=[
                ('id', models.AutoField(verbose_name='ID', auto_created=True, serialize=False, primary_key=True)),
                ('date_of_birth', models.DateField(null=True, blank=True)),
                ('activation_key', models.CharField(max_length=40, null=True, blank=True)),
                ('key_expires', models.DateTimeField(null=True, blank=True)),
                ('photo', models.ImageField(upload_to='users/%Y/%m/%d', default='users/default_user_icon/default_user_icon.png', blank=True)),
                ('user', models.OneToOneField(to=settings.AUTH_USER_MODEL)),
            ],
        ),
    ]
