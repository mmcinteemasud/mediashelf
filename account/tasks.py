from __future__ import absolute_import

from celery import task
from django.contrib.auth import get_user_model
from django.template.loader import render_to_string
from django.utils.html import strip_tags
import smtplib
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
from account.models import Profile

@task
def EmailVerification(user_id):
	# Define to/from
	user=get_user_model().objects.get(pk=user_id)
	user_profile=user.profile
	sender = 'MediaShelf <admin@getmediashelf.com>'
	recipient = user.email
	# Create message
	htmlcontent = render_to_string('registration/email_verification.html', {'user': user.first_name, 'activation_key': user_profile.activation_key})
	textcontent = strip_tags(htmlcontent)
	msg = MIMEMultipart('alternative')
	msg['Subject'] = "Account verification"
	msg['From'] = sender
	msg['To'] = recipient
	mssgplain = MIMEText(textcontent, 'plain')
	mssghtml = MIMEText(htmlcontent, 'html')
	msg.attach(mssgplain)
	msg.attach(mssghtml)

	# Create server object with SSL option
	server = smtplib.SMTP_SSL('smtp.zoho.com', 465)

	# Perform operations via server
	server.login('admin@getmediashelf.com', 'Amarachi1')
	server.sendmail(sender, [recipient], msg.as_string())
	server.quit()
	return 'Email Sent'
